FROM ubuntu

ENV PATH=/root/.local/bin:$PATH

RUN apt-get update -y
RUN apt-get install -y python3-pip python-dev
RUN pip3 install awscli

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip3 install -r requirements.txt

COPY . /app
ENTRYPOINT ["python"]
CMD ["project.py"]
